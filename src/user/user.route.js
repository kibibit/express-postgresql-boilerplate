const express = require('express')
const router = express.Router()
const User = require('./user.controller')
const auth = require('../middlewares/auth')

router
  .route('/:userId')
  .get(auth, User.BelongToCompany, User.getOne)
  .put(auth, User.BelongToCompany, User.Update)
  .delete(auth, User.BelongToCompany, User.Delete)

router.route('/signin').post(User.SignIn)

router.route('/signup').post(User.SignUp)

module.exports = router
