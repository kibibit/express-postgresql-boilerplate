const { PrismaClient } = require('@prisma/client')
const prisma = new PrismaClient()
const argon = require('argon2')
const jwt = require('jsonwebtoken')

const SignIn = async (req, res) => {
  const { email, password } = req.body
  try {
    const user = await prisma.user.findUnique({ where: { email } })
    if (user == null) return res.status(400).json({ error: 'credentials incorrect' })
    const isValidPassword = await argon.verify(user.password, password)
    if (!isValidPassword) return res.status(400).json({ error: 'credentials incorrect' })
    const token = await signToken(user.id, user.email, user.companyId)
    return res
      .status(200)
      .json({ name: user.name, id: user.id, companyId: user.companyId, email: user.email, token: token })
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

const signToken = async (userId, email, companyId) => {
  const payload = { userId, email, companyId }
  const secret = process.env.JWT_SECRET
  const token = await jwt.sign(payload, secret, { expiresIn: '24h' })
  return token
}

const SignUp = async (req, res) => {
  const { name, email, password, companyId } = req.body
  const hash = await argon.hash(password)
  try {
    const user = await prisma.user.create({
      data: { name: name, email: email, password: hash, companyId: companyId }
    })
    const token = await signToken(user.id, user.email, user.companyId)
    return res
      .status(200)
      .json({ name: user.name, id: user.id, companyId: user.companyId, email: user.email, token: token })
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

const getOne = async (req, res) => {
  try {
    const { password, ...data } = res.locals.user
    res.status(200).json(data)
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

const Update = async (req, res) => {
  const { name } = req.body
  const userId = res.locals.user.id
  try {
    const updatedUser = await prisma.user.update({ where: { id: userId }, data: { name } })
    const { password, ...data } = updatedUser
    return res.status(200).json(data)
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

const Delete = async (req, res) => {
  try {
    const userId = res.locals.user.id
    const deletedUser = await prisma.user.delete({ where: { id: userId } })
    const { password, ...data } = deletedUser
    return res.status(200).json(data)
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

const BelongToCompany = async (req, res, next) => {
  const userId = Number(req.params.userId)
  try {
    const user = await prisma.user.findFirst({ where: { id: userId, companyId: req.user.companyId } })
    if (user == null) return res.status(404).send({ error: 'No exist or belong to another company' })
    res.locals.user = user
    next()
  } catch (eer) {
    return res.status(500).json({ message: eer.message })
  }
}

module.exports = { getOne, Update, Delete, SignIn, SignUp, BelongToCompany }
